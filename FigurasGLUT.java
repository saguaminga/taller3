package org.aguaminga;

import com.sun.opengl.util.Animator;
import com.sun.opengl.util.GLUT;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
import javax.media.opengl.glu.GLUquadric;


/**FigurasGLUT 
 * author: Alejandra Guaminga
 * 2020-07-21
 */
public class FigurasGLUT implements GLEventListener {
    
    static float x,y;
    static float ang = 0;
    float a;
   
    static  Frame frame = new Frame("FIGURAS");
    
    public static void main(String[] args) {
       
        GLCanvas canvas = new GLCanvas();

        canvas.addGLEventListener(new FigurasGLUT());
        frame.add(canvas);
        frame.setSize(700,700);
        final Animator animator = new Animator(canvas);
        frame.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                // Run this on another thread than the AWT event queue to
                // make sure the call to Animator.stop() completes before
                // exiting
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        animator.stop();
                        System.exit(0);
                    }
                }).start();
            }
        });
        // Center frame
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        animator.start();
    }

    @Override
    public void init(GLAutoDrawable drawable) {
        // Use debug pipeline
        // drawable.setGL(new DebugGL(drawable.getGL()));

        GL gl = drawable.getGL();
        System.err.println("INIT GL IS: " + gl.getClass().getName());

        // Enable VSync
        gl.setSwapInterval(1);

        // Setup the drawing area and shading mode
        gl.glClearColor(0.84f, 0.84f, 0.84f, 0.0f); //Fondo
        gl.glShadeModel(GL.GL_SMOOTH); // try setting this to GL_FLAT and see what happens.
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();

        if (height <= 0) { // avoid a divide by zero error!
        
            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(45.0f, h, 1.0, 20.0);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        GL gl = drawable.getGL();
        GLU glu = new GLU();
        GLUT glut = new GLUT();
        
        // Clear the drawing area
        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();

        gl.glTranslated(0.0f, 0.0f, -18.0f);
        GLUquadric quad;
        quad =glu.gluNewQuadric();
        glu.gluQuadricDrawStyle(quad,GLU.GLU_FILL);

//           
            //Esfera s�lida          
           gl.glPushMatrix();
            gl.glColor3f(0.74f, 0.428f, 0.835f); 
            gl.glTranslatef(-6.0f,5.5f, 0.0f);
            gl.glRotatef(ang, -1, 0, 0);
            glut.glutSolidSphere(1, 48, 48);
           gl.glPopMatrix();
           //Esfera en malla
           gl.glPushMatrix();
             gl.glColor3f(0.74f, 0.428f, 0.835f); 
            gl.glTranslatef(-3.5f,5.5f, 0.0f);
            gl.glRotatef(ang, 0, -1, 0);
            glut.glutWireSphere(1, 48, 48);
           gl.glPopMatrix();
                        
           //Cono s�lido
           gl.glPushMatrix();
            gl.glColor3f(0.64f, 0.4794f, 0.3696f); 
            gl.glTranslatef(6.0f,-2.75f, 0.0f);
            gl.glRotatef(ang, 1, 0, 0);
            gl.glRotatef(ang, 0, -1, 0);
            glut.glutSolidCone(1, 1, 50, 50);
           gl.glPopMatrix();
           //Cono en malla
           gl.glPushMatrix();
           gl.glColor3f(0.64f, 0.4794f, 0.3696f); 
            gl.glTranslatef(3.5f,-2.75f, 0.0f); 
            gl.glRotatef(ang, -1, 0, 0);
            gl.glRotatef(ang, 0, 1, 0);
            glut.glutWireCone(1,1, 50, 50);
           gl.glPopMatrix();
        
        //Dodecaedro s�lido
          gl.glPushMatrix();
            gl.glColor3f(1.0f, 0.768f, 0.0f); 
            gl.glTranslatef(-3.5f,0.0f, 0.0f);
            gl.glScalef(0.6f,0.6f, 0.6f);
            gl.glRotatef(ang, -1, 0, 0);
            glut.glutSolidDodecahedron();
           gl.glPopMatrix();
           //Dodecaedro en malla
          gl.glPushMatrix();
            gl.glColor3f(1.0f, 0.768f, 0.0f); 
            gl.glTranslatef(-6f,0.0f, 0.0f);
            gl.glScalef(0.6f,0.6f, 0.6f);
            gl.glRotatef(ang, 0, -1, 0);
            glut.glutWireDodecahedron();
           gl.glPopMatrix();
           
            //Icosaedro s�lido
          gl.glPushMatrix();
            gl.glColor3f(0.666f, 0.4488f, 0.0156f);
            gl.glTranslatef(-6.0f,-2.75f, 0.0f);
           gl.glRotatef(ang, -1, 0, 0);
            glut.glutSolidIcosahedron();
           gl.glPopMatrix();
           //Icosaedro en malla
          gl.glPushMatrix();
            gl.glColor3f(0.666f, 0.4488f, 0.0156f);
            gl.glTranslatef(-3.5f,-2.75f, 0.0f);
//            gl.glScalef(0.5f,0.5f, 0.5f);
            gl.glRotatef(ang, 0, -1, 0);
            glut.glutWireIcosahedron();
           gl.glPopMatrix();
//            
        //Dona s�lida
          gl.glPushMatrix();
            gl.glColor3f(0.42f, 0.217f, 0.005f); 
            gl.glTranslatef(-6.0f,-5.5f, 0.0f);
            gl.glScaled(0.7, 0.7, 0.7);
            gl.glRotatef(ang, -1, 0, 0);
            glut.glutSolidTorus(0.5,1, 50, 50);
           gl.glPopMatrix();
           //Dona en malla
          gl.glPushMatrix();
            gl.glColor3f(0.42f, 0.217f, 0.005f );
            gl.glTranslatef(-3.5f,-5.5f, 0.0f);
            gl.glScaled(0.7, 0.7, 0.7);
            gl.glRotatef(ang, 0, -1, 0);
            glut.glutWireTorus(0.5,1, 50, 50);
           gl.glPopMatrix();
                         
            //Cubo s�lido
           gl.glPushMatrix();
            gl.glColor3f(0.4178f, 0.55f, 0.195f);
            gl.glTranslatef(6.0f,5.5f, 0.0f);
            gl.glRotatef(ang, 0, 1, 0);
            glut.glutSolidCube(1.5f);
           gl.glPopMatrix();
           //Cubo en malla
           gl.glPushMatrix();
            gl.glColor3f(0.4178f, 0.55f, 0.1955f);
            gl.glTranslatef(3.5f,5.5f, 0.0f);
            gl.glRotatef(ang, 1, 0, 0);
            glut.glutWireCube(1.5f);
           gl.glPopMatrix();
//           
            //Tetraedro s�lido
          gl.glPushMatrix();
            gl.glColor3f(0.4136f, 0.47f, 0.479f); 
            gl.glTranslatef(6.0f,2.75f, 0.0f);
            gl.glRotatef(45, 0, 1, 0);
            gl.glRotatef(ang, 0, 1, 0);
            glut.glutSolidTetrahedron();
           gl.glPopMatrix();
          //Tetraedro en malla
          gl.glPushMatrix();
             gl.glColor3f(0.4136f, 0.47f, 0.479f); 
            gl.glTranslatef(3.5f,2.75f, 0.0f);
           gl.glRotatef(ang, 1, 0, 0);
            glut.glutWireTetrahedron();
           gl.glPopMatrix();
//           
           //Octaedro s�lido
          gl.glPushMatrix();
            gl.glColor3f(0.5467f, 0.487f, 0.71f); 
            gl.glTranslatef(3.5f,0.0f, 0.0f);
            gl.glRotatef(ang, 0, 1, 0);
            glut.glutSolidOctahedron();
           gl.glPopMatrix();
             //Octaedro en malla
          gl.glPushMatrix();
             gl.glColor3f(0.5467f, 0.487f, 0.71f); 
            gl.glTranslatef(6.0f,0.0f, 0.0f);
            gl.glRotatef(ang, 1, 0, 0);
            glut.glutWireOctahedron();
           gl.glPopMatrix();
            
           //Disco s�lido
           gl.glPushMatrix();
            gl.glColor3f(0.442f, 0.0f, 0.225f); 
            gl.glTranslatef(-6.0f,2.75f, 0.0f);
            gl.glRotatef(ang, 1, 0, 0);
            gl.glRotatef(ang, 0, -1, 0);
            glu.gluDisk(quad, 0.5, 1, 50, 50);
           gl.glPopMatrix();
           //Disco Parcial S�lido
           gl.glPushMatrix();
           gl.glColor3f(0.442f, 0.0f, 0.225f); 
            gl.glTranslatef(-3.5f,2.75f, 0.0f);
            gl.glRotatef(ang, -1, 0, 0);
            gl.glRotatef(ang, 0, 1, 0);
            glu.gluPartialDisk(quad, 0.5, 1, 50, 50, 90, 180);
           gl.glPopMatrix();
           
          //Dodecaedro r�mbico s�lido
          gl.glPushMatrix();
            gl.glColor3f(0.3f, 0.29f, 0.2777f);
            gl.glTranslatef(6.0f,-5.5f, 0.0f);
            gl.glRotatef(ang, 0, 1, 0);
            glut.glutSolidRhombicDodecahedron();
           gl.glPopMatrix();
//            
           //Dodecaedro r�mbico en malla
          gl.glPushMatrix();
            gl.glColor3f(0.3f, 0.29f, 0.2777f);
            gl.glTranslatef(3.5f,-5.5f, 0.0f);
            gl.glRotatef(ang, 1, 0, 0);
            glut.glutWireRhombicDodecahedron();
           gl.glPopMatrix();
//            
           //Tetera s�lida
          gl.glPushMatrix();
            gl.glColor3f(0.22f, 0.0f, 0.0933f);
            gl.glTranslatef(0.0f,4.5f, 0.0f);
            gl.glRotatef(ang, 0, 0, 1);
            gl.glRotatef(ang, 0, 1, 0);
            glut.glutSolidTeapot(0.95);
           gl.glPopMatrix();
//           
            //Tetera en malla
          gl.glPushMatrix();
            gl.glColor3f(0.22f, 0.0f, 0.0933f);
            gl.glTranslatef(0.0f,-4.5f, 0.0f);
            gl.glRotatef(ang, 0, 0, 1);
            gl.glRotatef(ang, 1, 0, 0);
            glut.glutWireTeapot(0.95);
           gl.glPopMatrix();
           
//            Tubo s�lido
           gl.glPushMatrix();
            gl.glColor3f(0.0f, 0.446f, 0.0465f); 
            gl.glTranslatef(0.0f,-1.0f, 0.0f);
            gl.glScaled(0.8, 0.8, 0.8);
            gl.glRotatef(ang, -1, 0, 0);
            gl.glRotatef(ang, 0, -1, 0);
            glu.gluCylinder(quad, 1, 1, 2, 36,36);
           gl.glPopMatrix();
           //Tubo en malla
            gl.glPushMatrix();
            gl.glColor3f(0.0f, 0.446f, 0.0465f); 
            gl.glTranslatef(0.0f,1.5f, 0.0f);
            gl.glScaled(0.8, 0.8, 0.8);
            gl.glRotatef(ang, 1, 0, 0);
            gl.glRotatef(ang, 0,1, 0);
            glut.glutWireCylinder(1, 2, 50, 50);
           gl.glPopMatrix();
        gl.glFlush();
        ang+= 0.7;
    }

    @Override
    public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
    }
}

