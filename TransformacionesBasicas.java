package org.aguaminga;

import com.sun.opengl.util.Animator;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.glu.GLU;
import javax.swing.JFrame;



/**
 * MiFigura.java <BR>
 * Autora: Alejandra Guaminga
 *
 * 2020-07-16
 */
public class TransformacionesBasicas extends JFrame implements KeyListener {
 public TransformacionesBasicas() {
        setSize(700, 700);
        setLocation(10, 40);
        setTitle("Uso B�sico de Tranformaciones");
        GraphicListener listener = new GraphicListener();
        GLCanvas canvas = new GLCanvas(new GLCapabilities());
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);        
        Animator animator = new Animator(canvas);
        animator.start();        
        addKeyListener(this);
    }

    public static void main(String args[]) {
        TransformacionesBasicas myframe = new TransformacionesBasicas();
        myframe.setVisible(true);
        myframe.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    //Variables para instanciar funciones de opengl
    static GL gl;
    static GLU glu;

    //Variables para las transformaciones
    private static float trasladaX=0;
    private static float trasladaY=0;
    private static float escalaX=1;
    private static float escalaY=1;
    private static float rotarX=0;
    private static float rotarY=0;

    @Override
    public void keyTyped(KeyEvent ke) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public class GraphicListener implements GLEventListener {

        @Override
        public void display(GLAutoDrawable arg0) {
            //Inicializamos las variables
            glu = new GLU();
            gl = arg0.getGL();

            gl.glClear(GL.GL_COLOR_BUFFER_BIT);
            //color de fondo de la ventana 
            gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);            
            //par�metros de proyecci�n
            gl.glMatrixMode(GL.GL_PROJECTION);
            gl.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
            //grosor de l�neas
            gl.glLineWidth(1.5f);
            //el tama�o de puntos
            gl.glPointSize(3.0f);
            //Matriz identidad
            gl.glLoadIdentity();

            //movimiento cuadrado
            gl.glTranslatef(trasladaX, trasladaY, 0);
            gl.glScalef(escalaX, escalaY, 0);
            gl.glRotatef(rotarX, 1, 0, 0);
            gl.glRotatef(rotarY, 0, 1, 0);
            gl.glColor3f(0.4071f, 0.0203f, 0.2874f);
            gl.glBegin(GL.GL_QUADS);
                gl.glVertex2f(0, 0);
                gl.glVertex2f(0.25f, 0);
                gl.glVertex2f(0.25f, 0.25f);
                gl.glVertex2f(0, 0.25f);
            gl.glEnd();
         
            gl.glFlush();
        }

        @Override
        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
        }

        //En este metodo se queda el mismo segmento que hemos venido manejando
        @Override
        public void init(GLAutoDrawable arg0) {
            GL gl = arg0.getGL();
            gl.glEnable(GL.GL_BLEND);
            gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        }

        @Override
        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        }
    }

  
 @Override
    public void keyPressed(KeyEvent e) {
        //eventos de teclado
        if(e.getKeyCode() == KeyEvent.VK_RIGHT){
            trasladaX+=.1f;
            System.out.println("Valor en la traslaci�n de X: " + trasladaX);
        }

        if(e.getKeyCode() == KeyEvent.VK_LEFT){
            trasladaX-=.1f;
            System.out.println("Valor en la traslaci�n de X: " + trasladaX);
        }

        if(e.getKeyCode() == KeyEvent.VK_UP){
            trasladaY+=.1f;
            System.out.println("Valor en la traslaci�n de Y: " + trasladaY);
        }

        if(e.getKeyCode() == KeyEvent.VK_DOWN){
            trasladaY-=.1f;
            System.out.println("Valor en la traslaci�n de Y: " + trasladaY);
        }

        if(e.getKeyCode() == KeyEvent.VK_D){
            escalaX+=1f;
            System.out.println("Valor en la escalaci�n en X: " + escalaX);
        }

        if(e.getKeyCode() == KeyEvent.VK_A){
            escalaX-=1f;
            System.out.println("Valor en la escalaci�n en X: " + escalaX);
        }

         if(e.getKeyCode() == KeyEvent.VK_R){
            rotarX+=1f;
            System.out.println("Valor en la rotaci�n en X: " + rotarX);
        }

        if(e.getKeyCode() == KeyEvent.VK_T){
            rotarX-=1f;
            System.out.println("Valor en la rotaci�n en X: " + rotarX);
        }

        if(e.getKeyCode() == KeyEvent.VK_Y){
            rotarY+=1f;
            System.out.println("Valor en la rotaci�n en Y: " + rotarY);
        }

        if(e.getKeyCode() == KeyEvent.VK_U){
            rotarY-=1f;
            System.out.println("Valor en la rotaci�n en Y: " + rotarY);
        }

         if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
            trasladaX=0;
            trasladaY=0;
            escalaX=1;
            escalaY=1;
            rotarX=0;
            rotarY=0;
        }
    }

 
}
